#!/usr/bin/python
# -*- coding: utf-8 -*-

print ( """
╔═╗╔═╗╔═══╗╔════╗╔════╗╔╗─╔╗╔═══╗──────     ╔═══╗╔═══╗╔╗──╔╗╔═══╗╔═══╗
║║╚╝║║║╔═╗║║╔╗╔╗║║╔╗╔╗║║║─║║║╔══╝──────     ║╔═╗║║╔═╗║║╚╗╔╝║║╔═╗║║╔══╝
║╔╗╔╗║║║─║║╚╝║║╚╝╚╝║║╚╝║╚═╝║║╚══╗╔╗╔╗╔╗     ║╚═╝║║╚═╝║╚╗╚╝╔╝║║─╚╝║╚══╗
║║║║║║║╚═╝║──║║────║║──║╔═╗║║╔══╝║╚╝╚╝║     ║╔══╝║╔╗╔╝─╚╗╔╝─║║─╔╗║╔══╝
║║║║║║║╔═╗║──║║────║║──║║─║║║╚══╗╚╗╔╗╔╝     ║║───║║║╚╗──║║──║╚═╝║║╚══╗
╚╝╚╝╚╝╚╝─╚╝──╚╝────╚╝──╚╝─╚╝╚═══╝─╚╝╚╝─     ╚╝───╚╝╚═╝──╚╝──╚═══╝╚═══╝
______________________________________________________________________
Script criado por Matthew Pryce na versao 2.7.10 do Python
______________________________________________________________________
O script junta texto com cadeias de caracteres especiais ou numeros
______________________________________________________________________
O script pega o que foi digitado na 1° e 2° opcao e transforma em MD5
______________________________________________________________________
___________________________SEJA___BEM__VINDO__________________________
""")


import md5, binascii

nome = '?'

while nome:
    m = md5.new()
    nome = raw_input('Digite as palavras que desejesa criptografar (ou de ENTER para encerrar): \n')
    if nome:
        m.update(nome)
        senha = raw_input('Digite numeros letras ou caracteres para criar uma criptografia forte: \n')
        m.update(senha)
        print 'MD5: ', binascii.b2a_base64(m.digest())
